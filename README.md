This is a JIRA Server App which integrats JIRA Server with Robotic Process Automation Tool Uipath.

**What it is ?**

This obr file is JIRA SERVER ADD-ON which can be installed using Universal Plugin Manager.


**What it provides?**

It provide a post-function in JIRA which will let you configure a Uipath automation BOT for workflow transition. When the particular transition
happens then the Uipath Bot will be triggered to execute/run.

User can provide conditions in the form of AND and OR to run the BOT bases on the conditions while configuring post-function.

User can pass the data from JIRA to Uipath BOT in the form of Assets post-function.


**Limitations :**

1. Assets in UIPath can only hold one value , hence user need to be careful to provide only those value which have one value not multiple. 
2. Only override Assets that have String values.
3. These system fields are supported for conditions : 
4. summary ,issuetype, affectversion, fixversion, assignee, reporter, priority

5. In conditions ,  date field value need to be specified in perfect format "yyyy-MM-dd  hh:mm:ss". Need more tests to confirm. (change Jira date format and then test)
6. In conditions , only equal to (=) is supported and no other operation like (>,<,! etc not supported)


**Steps to Configure Post-Function :**

1. Add Post-Function : Click on Add PostFunction in workflow configurator and select the one with following description "UiPath Trigger Post Function"
2. Define Precondition : Define condition that must be true for the Uipath BOT to run. You will have 2 text boxes in which you can define conditions in comma separated format. 
   The commas will translate to OR while the relationship between two text box will translate to AND. If you do not wish to provide condition then you can leave them empty.

  		   example : condition 1 : customfield_10002=option1,summary=test

		 		     condition 2 : customfield_10003=option2,issuetype=bug
		  
            		 Resulting condition : (customfield_10002=option1 OR summary=test) AND (customfield_10003=option2 OR issuetype=bug)
		  
3. Provide Uipath Orchestrator Credentials. It will fetch the data about your existing BOTs and Assets in UiPath.Click Next.
4. Select the BOT you want to run. You can override upto 3 Assets of Uipath with constant values or with JIRA field value. This is the way to pass
   data from JIRA to Uipath. Save Post Function.

Now you can test it with transitioning the JIRA issue which can use the postfunction.
Screenshot for the above 4 steps are added in the repository for reference.



**My Contact Details :**

mail : prax0724@gmail.com
linkedin : https://www.linkedin.com/in/prakhar-srivastav-35214518/